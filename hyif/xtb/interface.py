import pathlib
import warnings
from typing import Any, Dict, Optional, Tuple

import numpy as np
from hyset import ComputeSettings, create_compute_settings

from ..importme import HylleraasInterface
from ..runners import RunnerFileStdout
from ..utils import unique_filename
from .parser import Xtbexternal, XtbFast

# gradients
# reaction_path
# geometry_optimization
# constraints
# input
# args


class Xtb(HylleraasInterface):
    """Main XTB class."""

    def __init__(self, method: dict):

        self.compute_settings: ComputeSettings = method.setdefault('compute_settings', create_compute_settings())
        self.parser_type = method.get('parser', 'uxtbpy')

        # self.runner: Runners
        # self.parser: XtbOutput

        self.runner = RunnerFileStdout('xtb', self.compute_settings)
        self.parser = XtbFast(self.parser_type)

        self.input: str = method.get('input', '')
        self.args = Xtb.get_args_from_method(method.get('args', []), method)

    def run(self, molecule: Any, options: Optional[dict] = None):
        """Run calculation in xtb.

        Parameter
        ---------
        molecule : Molecule
            hylleraas molecule instance
        options : dict
            dictionary with additional options, e.g.
                options={'constrain': ['angle', 1, 0, 2, 106]}
            or
                options={'constrain': [['force constant', 0.1 ], ['distance', 1, 0, 5.0]]}

        Note
        ----
            default force constant is 0.05.

        Returns
        -------
        dict
            parsed results

        """
        filename = self.prep_mol(molecule)
        args, input_str, filename_input = self.gen_args(self.args, filename, self.input, options)

        # remove restart
        restart_file = self.construct_wdpath('xtbrestart')
        if restart_file.exists():
            restart_file.unlink()

        # generate unique outputfilename from args ( = options + xyz filename + extra inputfileame)

        filename_out = unique_filename(args)

        output_path = self.construct_wdpath(filename_out + '.out')

        if any(['--hess' in args, '--ohess' in args]):
            path1 = self.construct_wdpath('hessian')
            if path1.exists():
                path1.unlink()
            path2 = self.construct_wdpath('g98.out')
            if path2.exists():
                path2.unlink()

        if output_path.exists():
            # with open(output_path, 'r') as output_file:
            #     output = output_file.read()
            output_path.unlink()
            output = self.runner.run(*args)
            with open(output_path, 'wt') as output_file:
                output_file.write(output)
        else:

            output = self.runner.run(*args)
            with open(output_path, 'wt') as output_file:
                output_file.write(output)

        result = self.parser.parse(output)
        result['filename'] = filename_out
        result['input_file'] = filename_input
        result['args'] = args
        result['additional_input'] = input_str

        if '--opt' in args:
            result['xtbopt.xyz'] = Xtbexternal.read_final_geometry(self.construct_wdpath('xtbopt.xyz'))

        if '--grad' in args:
            result['gradient'] = Xtbexternal.read_gradient(self.construct_wdpath(filename + '.engrad'))

        if any(['hess' in arg for arg in args]):
            result['hessian'] = Xtbexternal.read_hessian(self.construct_wdpath('hessian'))
            result['frequencies'] = Xtbexternal.read_frequencies(self.construct_wdpath('g98.out'))
            result['normal_modes'] = Xtbexternal.read_normal_modes(self.construct_wdpath('g98.out'))

        return result

    def gen_args(self, arglist: list, filename: str, input_str: str, options: dict) -> Tuple[list, str, str]:
        """Generate argument list for xtb."""
        for i, arg in enumerate(arglist):
            if '.xyz' in arg:
                self.previous = arglist.pop(i)

        if options is not None:
            arglist = Xtb.get_args_from_list(arglist, [options])
            input_str += Xtb.get_constraints(options)

        arglist.insert(0, str(filename) + '.xyz')
        filename_input = ''
        if input_str:
            filename_input = self.write_input(input_str, '.inp')
            if '--input' in arglist:
                i = arglist.index('--input')
                del arglist[i:i + 2]
            arglist.insert(0, f'{filename_input}')
            arglist.insert(0, '--input')

        return arglist, input_str, filename_input

    def prep_mol(self, molecule: Any):
        """Prepare molecule input for xtb."""
        if isinstance(molecule, dict):
            prop = molecule.get('properties', None)
            mol_string = Xtb.gen_molecule_str(molecule['atoms'], molecule['coordinates'])
        else:
            try:
                prop = molecule.properties
                mol_string = Xtb.gen_molecule_str(molecule.atoms, molecule.coordinates)
            except Exception:
                raise Exception(f'could not process molecule {molecule}')

        properties_dict = {'charge': '--chrg', 'unpaired_electrons': '--uhf'}
        if 'charge' in prop:
            charge = prop['charge']
            if properties_dict['charge'] not in self.args:
                self.args.append(properties_dict['charge'] + ' ' + str(charge))

        nunpaired = 0
        if 'unpaired_electrons' in prop:
            nunpaired = prop['unpaired_electrons']
        elif 'spin_projection' in prop:
            nunpaired = int(2. * prop['spin_projection'])
        elif 'spin_multiplicity' in prop:
            nunpaired = int(prop['spin_multiplicity'] - 1)

        if nunpaired > 0:
            for j, arg in enumerate(self.args):
                if properties_dict['unpaired_electrons'] in arg:
                    del self.args[j]
            print(self.args)
            self.args.append(properties_dict['unpaired_electrons'] + ' ' + str(nunpaired))

        filename = unique_filename(mol_string.split('\n'))
        with open(self.construct_wdpath(filename + '.xyz'), 'wt') as coord_file:
            coord_file.write(mol_string)

        return filename

    @classmethod
    def get_args_from_method(cls, arglist: list, method: dict) -> list:
        """Get and Set arguments for xtb."""
        if not all([isinstance(arg, str) for arg in arglist]):
            raise Exception('input error: arglist should only contain strings')

        qcmethods = {'gfn-xtb1': '--gfn 1', 'gfn-xtb2': '--gfn 2', 'gfn-ff': '--gfnff'}
        if 'qcmethod' in method:
            if method['qcmethod'].lower() in qcmethods:
                # omit defaults
                if not method['qcmethod'].lower() == 'gfn-xtb2':
                    arglist.append(qcmethods[method['qcmethod']])

        if 'properties' in method:
            proplist = [method['properties']] if not isinstance(method['properties'], list) else method['properties']
            arglist = Xtb.get_args_from_list(arglist, proplist)

        return arglist

    @classmethod
    def get_constraints(cls, input_dict: dict) -> str:
        """Read constraints from input dict."""
        constrain_str = ''
        for x in input_dict.keys():
            if 'constrain' not in x:
                continue
            constrain_str = '$write\n distances=true\n angles=true\n torsions=true\n$end\n'

            if not isinstance(input_dict[x], list):
                warnings.warn(f"list excepted (e.g. ['dihedral', 0, 1, 2, 3, 179.0]), got {type(input_dict[x])}")

            y = [input_dict[x]] if not isinstance(input_dict[x][0], list) else input_dict[x]
            force_constant = '0.05'
            for i, z in enumerate(y):
                if not isinstance(z[0], str):
                    warnings.warn(f"string excepted (e.g. ['dihedral', 0, 1, 2, 3, 179.0]), got {type(z[0])}")
                if 'force' in z[0]:
                    force_constant = z[1]
                    del y[i]
            constrain_str += f'$constrain\n force constant = {force_constant}\n'
            for z in y:
                constrain_str += ' ' + z[0] + ': '
                for i in range(1, len(z) - 1):
                    atom_nr = z[i] + 1
                    constrain_str += str(atom_nr) + ', '

                val = z[-1]
                constrain_str += str(val) + '\n'
            constrain_str += '$end'
        return constrain_str

    @classmethod
    def get_args_from_list(cls, arglist: list, input_list: list) -> list:
        """Get arguments from properties list."""
        argmap = {'geometry_optimization': '--opt', 'hessian': '--hess', 'gradient': '--grad'}

        for x in input_list:
            if isinstance(x, str):
                if x in argmap.keys():
                    Xtb.add_arg_xtb(arglist, argmap[x])
            elif isinstance(x, dict):
                if 'geometry_optimization' in x.keys():
                    Xtb.add_arg_xtb(arglist, '--opt')
                    conv = x['geometry_optimization']
                    arglist.append(f'{conv}')
                if 'fermi_smearing' in x.keys():
                    Xtb.add_arg_xtb(arglist, '--etemp')
                    temp = x['fermi_smearing']
                    arglist.append(f'{temp}')

                if 'properties' in x.keys():
                    y = [x['properties']] if not isinstance(x['properties'], list) else x['properties']
                    arglist = Xtb.get_args_from_list(arglist, y)

        return arglist

    def reaction_path(self, mol_start, mol_end, input: Optional[str] = None):
        """Compute reaction path."""
        input_dict: Dict = {
            'nrun': 1, 'npoint': 25, 'anopt': 10, 'kpush': 0.003, 'kpull': -0.015, 'ppull': 0.05, 'alp': 1.2
        }
        if input is None:
            is_file = False
        if isinstance(input, str):
            if not pathlib.Path(input).exists():
                is_file = False
                if '$path' not in input:
                    raise Exception(f'path input required in {input}')
            else:
                is_file = True
        elif isinstance(input, dict):
            is_file = False
            input_dict.update(input)

        if not is_file:
            input_str = '$path\n'
            for key, val in input_dict.items():
                input_str += key + '=' + str(val) + '\n'
            # input_str += json.dumps(input_dict, sort_keys=True)
            input_str += '$end'
            filename = unique_filename(input_str.split()) + '.inp'
            path = pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(filename)
            with open(path, 'wt') as input_file:
                input_file.write(input_str)

        start_file = self.prep_mol(mol_start)
        end_file = self.prep_mol(mol_end)

        self.args.insert(0, f'{filename}')
        self.args.insert(0, '--input ')

        self.args.insert(0, str(end_file))
        self.args.insert(0, '--path')
        self.args.insert(0, str(start_file))

        return self.runner.run(*self.args)

    def get_energy(self, molecule):
        """Compute energy using xtb."""
        result = self.run(molecule)
        return result['energy']

    def get_gradient(self, molecule):
        """Compute gradient using xtb."""
        # if 'grad' not in self.args:
        #     self.args.append('--grad')
        result = self.run(molecule, options={'properties': 'gradient'})
        return result['gradient']

    def get_hessian(self, molecule):
        """Compute hessian using xtb."""
        # hess_str = '--ohess' if 'opt' in self.args else '--hess'
        # if hess_str not in self.args:
        #     self.args.append(hess_str)
        result = self.run(molecule, options={'properties': 'hessian'})
        return result['hessian']

    def get_normal_modes(self, molecule):
        """Compute normal modes using xtb."""
        result = self.run(molecule, options={'properties': 'hessian'})
        output = {'frequencies': result['frequencies'], 'normal_modes': result['normal_modes']}
        return output

    def construct_wdpath(self, filename: str) -> pathlib.Path:
        """Construct path in work directiory."""
        return pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(filename)

    @classmethod
    def gen_molecule_str(cls, atoms, coordinates):
        """Generate  Molecule string in xyz format."""
        mol_string = str(len(atoms)) + '\n\n'
        for i, atom in enumerate(atoms):
            coord = np.array(coordinates).ravel().reshape(-1, 3)
            mol_string += atom
            mol_string += ' {:.12f}'.format(coord[i, 0])
            mol_string += ' {:.12f}'.format(coord[i, 1])
            mol_string += ' {:.12f}'.format(coord[i, 2])
            mol_string += '\n'

        return mol_string

    @classmethod
    def add_arg_xtb(cls, arglist: list, arg_new: str) -> None:
        """Add argument to xtb arglist."""
        geo_opt = ('--opt' in arglist)

        for i, arg in enumerate(arglist):
            if arg_new in arg:
                del arglist[i]
            if all(['hess' in arg_new, '--grad' in arg]):
                del arglist[i]
            if all(['grad' in arg_new, '--hess' in arg]):
                del arglist[i]

        if 'hess' in arg_new:
            arg_new = '--ohess' if geo_opt else '--hess'

        arglist.append(arg_new)
        return

    def write_input(self, input_str: str, suffix: Optional[str] = None) -> str:
        """Write input file to disc."""
        suffix = '' if suffix is None else suffix
        filename = unique_filename(input_str.split()) + suffix
        with open(self.construct_wdpath(filename), 'wt') as input_file:
            input_file.write(input_str)
        return filename

    @property
    def author(self):
        """Return authors email adress."""
        return 'tilmann.bodenstein@kjemi.uio.no'

    @property
    def version(self):
        """Return xtb program version."""
        return '6.5.1'


if __name__ == '__main__':

    myxtb = Xtb({'properties': {'geometry_optimization': 'tight'}, 'qcmethod': 'gfn-xtb2'})
    # out = myxtb.runner.run('--version')
    # print(out)
    myxtb2 = Xtb({})
    import hylleraas as hsp
    mol_start = hsp.Molecule('''15

C   -1.05403119  -0.85921125  -1.07844148
O   -0.74716995  -1.59204846   0.00037929
C    1.91999122   0.31825506  -0.65929558
C   -1.56348463   0.34378897  -0.70923752
C   -1.05432765  -0.85883374   1.07895685
C    1.92016161   0.31774885   0.65905212
C   -1.56373749   0.34366980   0.70888173
H   -0.86626022  -1.30691107  -2.03849048
H    2.21980037  -0.54462844  -1.23619995
H    1.61547946   1.18008308  -1.23636699
H   -1.89753571   1.13638114  -1.35561033
H   -0.86679445  -1.30614725   2.03907198
H    2.21960266  -0.54586684   1.23524723
H    1.61610877   1.17913244   1.23678680
H   -1.89780281   1.13623322   1.35526633''')
    mol_end = hsp.Molecule('''15

C   -0.33650300  -0.52567500  -1.05221900
O   -0.49920800  -1.44888700   0.00032300
C    1.08232400   0.03657400  -0.76729600
C   -1.29917500   0.57935400  -0.66347200
C   -0.33671300  -0.52527900   1.05252700
C    1.08262000   0.03575900   0.76715400
C   -1.29967800   0.57933100   0.66328300
H   -0.47204500  -0.99959700  -2.02194900
H    1.84062900  -0.63339500  -1.16910900
H    1.22478200   1.02637400  -1.19722100
H   -1.79017300   1.24152200  -1.35666900
H   -0.47213100  -0.99881000   2.02246200
H    1.84129300  -0.63425300   1.16825900
H    1.22479600   1.02528600   1.19777000
H   -1.79081700   1.24169700   1.35615700''')
    mymol = hsp.Molecule('H 0 0 1 \n H 0 0 0')
    mymol2 = hsp.Molecule('H 0 0 1.1 \n H 0 0 0')
    mymol3 = hsp.Molecule('O 0 0 0\n H 0 0 1.55 \n H 0 0.1 -1.55')
    mymol4 = hsp.Molecule({
        'atoms': ['O', 'H', 'H'], 'coordinates': [[1.96278518e-08, -4.41998786e-12, 3.99838905e-14],
                                                  [-1.59686973e-06, 1.76855012e-05, -9.39598553e-01],
                                                  [1.57724185e-06, -1.76854967e-05, 9.39598553e-01]]
    })

    # result= myxtb.run(mymol)
    # print(result)
    # test = myxtb2.reaction_path(mol_start, mol_end)
    # print(test)
    myxtb = Xtb({
        'properties': {
            'geometry_optimization': 'tight', 'constrain': [['force constant', 0.1], ['angle', 1, 0, 2, 130]]
        }, 'qcmethod': 'gfn-xtb2'
    })
    # myxtb = Xtb({'properties': 'gradient', 'qcmethod': 'gfn-xtb2'})
    # myxtb = Xtb({'properties': 'hessian'})
    # myxtb = Xtb({'input':'$constrain\n force constant=0.05\n angle: 5, 7, 8, 120.0'})
    # result = myxtb.run(mymol4)
    # print(result)
    myxtb = Xtb({})
    hess = myxtb.get_hessian(mymol4)
    print(hess)
    # print(dir(myxtb), myxtb.args)
    # result = myxtb.run(mymol3, options={'constrain': [['force constant', 0.6],
    # ['angle', 1, 0, 2, 150], ['dihedral', 0,1,2,3, 128]]})
    # print(result)
    mol = '''9

C -0.963726000000 -0.658530000000 -0.279914000000
Cl -2.254994000000 -0.623496000000 0.940995000000
C -0.057923000000 0.510272000000 -0.071511000000
C 1.253981000000 0.489055000000 0.211886000000
Br 2.267389000000 -1.068010000000 0.419667000000
Br 2.204266000000 2.093699000000 0.424984000000
H -1.434575000000 -0.593041000000 -1.265300000000
H -0.468524000000 -1.628329000000 -0.206056000000
H -0.545895000000 1.478381000000 -0.174750000000'''

    mymol = hsp.Molecule(mol)
    xtb = Xtb({'parser': 'full'})
    out = xtb.run(mymol)
    print(out)
    # 453, 796
