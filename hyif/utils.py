import collections.abc
import inspect
from typing import Any, List


def update_nested_dict(d, u):
    """Update (nested) dictionary d with dictionary u."""
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update_nested_dict(d.get(k, {}), v)
        else:
            d[k] = v
    return d


def key_in_arglist(*args, **kwargs) -> bool:
    """Look for keyword/attribute in input."""
    if 'look_for' not in kwargs:
        return False
    search_str = kwargs['look_for']

    if search_str in kwargs:
        return True

    for arg in args:
        if isinstance(arg, dict):
            if search_str in arg:
                return True
        elif inspect.isclass(type(arg)):
            if hasattr(arg, search_str):
                return True
    return False


def get_val_from_arglist(*args, **kwargs) -> Any:
    """Return value of keyword/attribute."""
    if 'look_for' not in kwargs:
        return None
    search_str = kwargs['look_for']

    if search_str in kwargs:
        return kwargs[search_str]
    for arg in args:
        if isinstance(arg, dict):
            if search_str in arg:
                return arg[search_str]
        elif inspect.isclass(type(arg)):
            if hasattr(arg, search_str):
                return getattr(arg, search_str)
    return None


def unique_filename(input_strings: List[str]) -> str:
    """Create a unique filename based on a list of input strings. Courtesy of daltonproject.

    Parameters
    ----------
    input_strings: List[str]
        List of input strings.

    Returns
    -------
    str:
        Unique filename.

    """
    import hashlib
    if not isinstance(input_strings, list):
        raise TypeError(f'Expected list, {type(input_strings).__name__} found.')
    for i, input_string in enumerate(input_strings):
        if not isinstance(input_string, str):
            raise TypeError(f'List item {i}: expected str instance, {type(input_string).__name__} found.')
    return hashlib.sha1(''.join(input_strings).encode('utf-8')).hexdigest()
