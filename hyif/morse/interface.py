# import sympy as sp


class Morse:
    """Classic Morse potential."""

    def __init__(self, method, molecule):

        self.De = 10
        self.k = 5
        self.re = 10
        # self.a = sqrt(k/2*De)

        if molecule.num_atoms != 2:
            raise Exception('Morse potential only defined for two-atomic molecules')

        molecule = self.molecule
        # method = self.method

    @staticmethod
    def morse_function(param_de, param_k, param_r, param_re):
        """Set up function."""
        pass
        # param_a = sqrt(param_k/2.*param_De)

        # De, a, r, r0 = sp.symbols('De a r r0')
        # morse = De*(1-exp(a*(r-r0)))**2

    def update_coordinates(self, coordinates):
        """Update coordinates.

        Parameters
        ----------
        coordinates : :obj:`array`
            coordinates

        Returns
        -------
        :obj:`array`
            updates coordinates

        """
        self.method.coordinates = coordinates.ravel().reshape(-1, 3)
        return self.method.coordinates

    # def get_energy(self):
    #     """Compute energy with LONDON.

    #     Returns
    #     -------
    #     :obj:`float`
    #         energy

    #     """
    #     write_file = open(self.filename_inp, 'w')
    #     input_str = LondonInput.gen_london_input(self.method, self.molecule)
    #     write_file.write(input_str)
    #     write_file.close()
    #     os.system(f'{self.executable} {self.filename_inp} > {self.filename_out}')
    #     energy = LondonOutput.london_output_parser_energy(self.filename_out)
    #     return energy


# equilibrium_distance
# well_depth
# force_constant

# De, a, r, r0  = symbols('De a r r0')
# init_printing(use_unicode=True)

# from sympy.vector import CoordSys3D
# C = CoordSys3D('C')
# v = 3*C.i + 4*C.j + 5*C.k
# print(sqrt(v.dot(v)))

# morse = De*(1-exp(a*( r-r0)))**2

# print(morse.diff(r))

# print(morse.subs(r,0))
# print(morse.subs(r,r0))

# from sympy.vector import CoordSys3D
# C = CoordSys3D('C')
# v = 3*C.i + 4*C.j + 5*C.k
# print(sqrt(v.dot(v)))
