from __future__ import annotations

from ..importme import HylleraasInterface

# try:
#     import something as st
#     st_found = True
# except Exception:
#     st_found = False


class Template(HylleraasInterface):
    """Template interface."""

    def __init__(self, method):

        # if not st_found:
        #     raise ImportError('package something not installed')

        self.initialize(method)

    def initialize(self, method):
        """Initialize the Interface class."""
        pass

    @classmethod
    def get_input_molecule(cls, *args):
        """Read molecule input from file. Not always needed."""
        pass

    @classmethod
    def get_input_method(cls, *args):
        """Read method input from file. Not always needed."""
        pass

    def restart(self, *args, **kwargs) -> None:
        """Nice to have. e.g. update coordinates with geometry optimization output."""
        pass

    def update_coordinates(self, coordinates):
        """Update coordinates. Required for geometry optimization."""
        pass

    def get_energy(self):
        """Get energy. Required for geometry optimization."""
        pass

    def get_gradient(self):
        """Get gradient. Required for geometry optimization."""
        return

    @property
    def version(self):
        """Set interface version."""
        return '0.0'

    @property
    def author(self):
        """Set author's name or email adress."""
        return 'foo.bar@myuniversity'
