import pathlib
from dataclasses import dataclass
from typing import Optional, Union

import numpy as np
import pycp2k
from hyset import ComputeSettings, create_compute_settings

from ..importme import HylleraasInterface
from ..runners import RunnerFileStdoutFile
from ..utils import unique_filename


class CP2K(HylleraasInterface):
    """CP2K interface."""

    def __init__(self, method: Union[dict, str, pathlib.Path]):

        self.compute_settings: ComputeSettings
        if isinstance(method, dict):
            self.compute_settings = method.setdefault('compute_settings', create_compute_settings())
            self.executable = method.setdefault('excecutable', 'cp2k.sopt')
            template = method.setdefault('template', 'cp2k_It1_000001.inp')
            self.options = method.setdefault('options', None)

        else:
            self.compute_settings = create_compute_settings()
            self.executable = 'cp2k.sopt'
            template = 'cp2k_It1_000001.inp'

        if isinstance(method, str) or isinstance(method, pathlib.Path):
            # could be input file
            pass

        # first check workdit
        work_dir = pathlib.Path(self.compute_settings.work_dir)
        curr_dir = pathlib.Path(__file__).parent

        template_check = work_dir / template
        self.template = template_check if template_check.exists() else None
        template_check = curr_dir / template
        self.template = template_check if template_check.exists() else None

        if not self.template:
            raise Exception('could not find template file')

        # move runner to compute settings
        self.runner = RunnerFileStdoutFile(self.executable, self.compute_settings)

        self.calc = pycp2k.CP2K()
        self.calc.parse(self.template)

        CP2K_INPUT = self.calc.CP2K_INPUT  # noqa N806
        GLOBAL = CP2K_INPUT.GLOBAL  # noqa N806
        FORCE_EVAL = CP2K_INPUT.FORCE_EVAL_list[0]  # noqa N806
        SUBSYS = FORCE_EVAL.SUBSYS  # noqa N806
        DFT = FORCE_EVAL.DFT  # noqa N806
        SCF = DFT.SCF  # noqa N806

        self.subsys = SUBSYS

        if self.options:
            for k, v in self.options.items():
                if self.is_int(v) or isinstance(v, int):
                    exec('%s = %d' % (k, int(v)))
                elif self.is_float(v) or isinstance(v, float):
                    exec('%s = %20.14f' % (k, float(v)))
                else:
                    exec("%s = '%s'" % (k, v))

    def run(self, molecule, unit_cell: Optional[Union[list, np.array]] = None, pbc: Optional[list] = None):
        """Run cp2k calculation."""
        ase_mol = [self.AseAtom(molecule.atoms[i], molecule.coordinates[i]) for i in range(len(molecule.atoms))]

        self.calc.create_coord(self.subsys, ase_mol)

        if pbc is not None and unit_cell is not None:
            self.calc.create_cell(self.subsys, self.AseCell(unit_cell, pbc))
        elif pbc is None and unit_cell is not None:
            self.subsys.CELL.A = unit_cell[0]
            self.subsys.CELL.B = unit_cell[1]
            self.subsys.CELL.C = unit_cell[2]

        input_str = self.calc.get_input_string()
        self.filename = unique_filename(input_str.split('\n'))
        infile = self.filename + '.inp'
        outfile = self.filename + '.out'
        work_dir = pathlib.Path(self.compute_settings.work_dir)
        inpath = work_dir / infile
        outpath = work_dir / outfile

        with open(inpath, 'wt') as input_file:
            input_file.write(input_str)

        if not outpath.exists():
            self.runner.run(infile, outfile)

        result = self.OutputParser.parse(outpath)

        result['input_file'] = inpath
        result['output_file'] = outpath

        return result

    class OutputParser:
        """CP2K output parser."""

        @classmethod
        def parse(cls, outpath):
            """Read cp2k output file."""
            with open(outpath, 'rt') as output_file:
                lines = output_file.readlines()
            for i, line in enumerate(lines):
                if 'Total number of' in line:
                    if '- Atoms:' in lines[i + 1]:
                        num_atoms = int(lines[i + 1].split()[-1])
                if 'ENERGY|' in line:
                    en = float(line.split()[-1])

                if 'ATOMIC FORCES in [a.u.]' in line:
                    istart = i + 3
                    gradient = []
                    for j in range(istart, istart + num_atoms):
                        line_split = lines[j].split()
                        xyz = [float(line_split[k]) for k in range(3, 6)]
                        gradient.append(xyz)

            result = {}
            result['energy'] = en
            result['gradient'] = np.array(gradient)
            result['output'] = '\n'.join(lines)
            return result

    def get_gradient(self,
                     molecule,
                     unit_cell: Optional[Union[list, np.array]] = None,
                     pbc: Optional[list] = None):
        """Compute gradient."""
        result = self.run(molecule, unit_cell, pbc)
        return result['gradient']

    def get_energy(self, molecule, unit_cell: Optional[Union[list, np.array]] = None, pbc: Optional[list] = None):
        """Compute energy."""
        result = self.run(molecule, unit_cell, pbc)
        return result['energy']

    def is_float(self, instr):
        """Check if string is float."""
        try:
            float(instr)
        except Exception:
            return False
        else:
            return True

    def is_int(self, instr):
        """Check if string is integer."""
        if isinstance(instr, int):
            return True
        return instr.isdigit()

    @dataclass
    class AseAtom:
        """Conversion."""

        symbol: str
        position: Union[list, np.array]

    class AseCell:
        """Required by pycp2k."""

        def __init__(self, cell, pbc):
            self.cell = cell
            self.pbc = pbc

        def get_cell(self):
            """Return unit cell."""
            return self.cell

        def get_pbc(self):
            """Return periodic boundary conditions."""
            return self.pbc

    @property
    def author(self):
        """Return who wrote this interface."""
        return 'tilmannb@uio.no'

    @property
    def version(self):
        """Return version this interface has been tested with."""
        return '7.1'


if __name__ == '__main__':

    import hylleraas as hsp
    mymol = hsp.Molecule('O')

    mycp2k = CP2K({'options': {'FORCE_EVAL.DFT.SCF.Max_scf': 300, 'FORCE_EVAL.Method': 'Quickstep'}})
    grad = mycp2k.get_gradient(mymol)
    print('gradient', grad)
    en = mycp2k.get_energy(mymol)
    print('energy', en)
