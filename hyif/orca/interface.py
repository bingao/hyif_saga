from __future__ import annotations

import os
import pathlib
from typing import Any, Dict, List, Optional, Tuple, Union

from hyset import ComputeSettings, create_compute_settings

from ..importme import HylleraasInterface
from ..runners import RunnerFileStdoutFile
from .parser import OrcaInput, OrcaMolecule, OrcaOutput

QCMETHODS: dict = {'rimp2': 'ri-mp2'}
PROPERTIES: dict = {
    'geometry_optimization': 'opt', 'gradient': 'engrad', 'numerical_gradient': 'numgrad',
    'numerical_hessian': 'numfreq', 'normal_modes': 'freq', 'analytical_hessian': 'freq',
    'transition_state': 'OptTS'
}


class Orca(HylleraasInterface):
    """Orca interface."""

    def __init__(self, method: Union[dict, str, pathlib.Path]):

        self.compute_settings: ComputeSettings
        if isinstance(method, dict):
            self.compute_settings = method.setdefault('compute_settings', create_compute_settings())
        else:
            self.compute_settings = create_compute_settings()

        self.runner = RunnerFileStdoutFile('orca', self.compute_settings)
        # note: get_method also sets hessian type
        self.main_input, self.specific_input = self.get_method(method)

        if 'MPI_NUM_PROCS' in self.compute_settings.env:
            num_procs = self.compute_settings.env.get('MPI_NUM_PROCS', 1)
            self.specific_input['PAL'] = 'NPROCS ' + str(num_procs)

    @property
    def version(self):
        """Return orca version."""
        return '5.0.3'

    @property
    def author(self):
        """Return author's email adress."""
        return 'tilmann.bodenstein@kjemi.uio.no'

    def get_method(self, method: Union[dict, str, pathlib.Path]):
        """Get method information in orca format."""
        if isinstance(method, dict):
            main_input = method.setdefault('main_input', [])
            specific_input = method.setdefault('specific_input', {})

            if 'qcmethod' in method:
                if isinstance(method['qcmethod'], str):
                    if method['qcmethod'].lower() in QCMETHODS:
                        qcmethod = QCMETHODS[method['qcmethod'].lower()]
                    else:
                        qcmethod = method['qcmethod'].lower()

                else:
                    if len(method['qcmethod']) == 1:
                        if method['qcmethod'].lower() in QCMETHODS:
                            qcmethod = QCMETHODS[method['qcmethod'].lower()]
                        else:
                            qcmethod = method['qcmethod'].lower()
                    else:  # DFT case@
                        qcmethod = [meth.lower() for meth in method['qcmethod']]

                if qcmethod not in main_input:
                    if isinstance(qcmethod, list):
                        main_input.extend(qcmethod)
                    else:
                        main_input.append(qcmethod)

            if 'basis' in method:
                if method['basis'].lower() not in main_input:
                    main_input.append(method['basis'].lower())

            if 'properties' in method:
                # should be a loop
                proplist = [method['properties']
                            ] if not isinstance(method['properties'], list) else method['properties']
                for x in proplist:
                    if x.lower() in PROPERTIES:
                        prop = PROPERTIES[method['properties'].lower()]
                    else:
                        prop = method['properties'].lower()
                    if prop not in main_input:
                        main_input.append(prop)

            self.hessian_type = method.get('hessian_type', 'analytical')
        else:
            try:
                main_input, specific_input = OrcaInput.read_method_from_file(str(method))
            except Exception:
                raise Exception(f'could not read orca input form file {method}')

        return main_input, specific_input

    @classmethod
    def get_input_molecule(cls, *args) -> OrcaMolecule:
        """Get molecule input."""
        molecule: dict = {}
        for arg in args:
            if isinstance(arg, str):
                if os.path.isfile(arg):
                    filename = os.path.join(os.getcwd(), arg)
                    molecule = OrcaInput.read_molecule_from_file(filename)

        return OrcaMolecule(atoms=molecule['atoms'],
                            coordinates=molecule['coordinates'],
                            properties=molecule['properties'])

    @classmethod
    def get_input_method(cls, *args) -> Tuple[List, Dict]:
        """Get method input."""
        main_input: List = []
        specific_input: Dict = {}
        for arg in args:
            if isinstance(arg, str):
                if os.path.isfile(arg):
                    filename = os.path.join(os.getcwd(), arg)
                    main_input, specific_input = OrcaInput.read_method_from_file(filename)

        return main_input, specific_input

    def add_keyword_orca(self, kw: Any) -> Tuple[list, dict]:
        """Add keyword to orca input objects."""
        main_input = self.main_input.copy()
        specific_input = self.specific_input.copy()
        if isinstance(kw, dict):
            if 'constrain' in kw.keys():
                constr = kw['constrain']
                constr = [constr] if not isinstance(constr, list) else constr
                constr = [constr] if not isinstance(constr[0], list) else constr

                const_list = ['  Constraints']
                for c in constr:
                    if not isinstance(c, list):
                        raise Exception("usage : ['dihedral', 1, 2, 0, 4, 120.] or ['cartesian', 4], etc. ")
                    if c[0] == 'distance':
                        cstr = '  { B ' + f'{c[1]} ' + f'{c[2]} ' + f'{c[3]} ' + ' C }'
                        const_list.append(cstr)
                    if c[0] == 'angle':
                        cstr = '  { A ' + f'{c[1]} ' + f'{c[2]} ' + f'{c[3]} ' + f'{c[4]} ' + ' C }'
                        const_list.append(cstr)
                    if c[0] == 'dihedrial':
                        cstr = '  { D ' + f'{c[1]} ' + f'{c[2]} ' + f'{c[3]} ' + f'{c[4]} ' + f'{c[5]} ' + ' C }'
                        const_list.append(cstr)
                    if c[0] == 'cartesian':
                        cstr = '  { C ' + f'{c[1] }' + ' C }'
                        const_list.append(cstr)
                const_list.append('  end')
                specific_input['geom'] = const_list

            if 'properties' in kw.keys():
                prop = kw['properties'].lower()
                if prop in PROPERTIES:
                    if prop not in main_input:
                        main_input.append(PROPERTIES[prop])

        # elif isinstance(kw, str):
        #     if kw.lower() in PROPERTIES:
        #         prop = PROPERTIES[kw.lower()]
        #     if prop not in main_input:
        #         main_input.append(prop)
        return main_input, specific_input

    def run(self, molecule, options: Optional[dict] = None):
        """Run an orca calculation.

        Parameter
        ---------
        molecule : Molecule
            hylleraas molecule instance
        options : dict
            dictionary with additional options, e.g.
                options={'constrain': ['angle', 1, 0, 2, 106]}
            or
                options={'constrain': [['distance', 1, 0, 5.0], ['cartesian', 5] ]}

        Returns
        -------
        dict
            parsed results

        """
        main_input = self.main_input
        specific_input = self.specific_input
        if options is not None:
            main_input, specific_input = self.add_keyword_orca(options)

        self.filename, input_str = OrcaInput.gen_input_orca(main_input, specific_input, molecule)
        outfile = self.filename + '.out'
        infile = self.filename + '.inp'
        inpath = pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(infile)
        with open(inpath, 'wt') as input_file:
            input_file.write(input_str)
        outpath = pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(outfile)
        if not outpath.exists():
            self.runner.run(infile, outfile)

        result = OrcaOutput.parse(outpath)

        return result

    def get_energy(self, molecule):
        """Compute energy with orca."""
        self.filename, input_str = OrcaInput.gen_input_orca(self.main_input, self.specific_input, molecule)
        outfile = self.filename + '.out'
        infile = self.filename + '.inp'

        inpath = pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(infile)
        with open(inpath, 'wt') as input_file:
            input_file.write(input_str)

        outpath = pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(outfile)
        if not outpath.exists():
            self.runner.run(infile, outfile)

        # energy = OrcaOutput.energy(path)

        # return energy
        result = OrcaOutput.parse(outpath)

        if result['warnings']:
            print(result['warnings'])
        return result['energy']

    def get_gradient(self, molecule):
        """Compute gradient with orca."""
        if 'engrad' not in self.main_input:
            self.main_input.append('engrad')

        self.filename, input_str = OrcaInput.gen_input_orca(self.main_input, self.specific_input, molecule)
        outfile = self.filename + '.out'
        infile = self.filename + '.inp'

        inpath = pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(infile)
        with open(inpath, 'wt') as input_file:
            input_file.write(input_str)

        path = pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(outfile)
        if not path.exists():
            self.runner.run(infile, outfile)

        gradients = OrcaOutput.gradient(path, molecule)

        return gradients

    def get_hessian(self, molecule):
        """Compute analytical hessian with orca."""
        if self.hessian_type == 'numerical':
            kw = 'numfreq'
        else:
            kw = 'freq'

        if kw not in self.main_input:
            self.main_input.append(kw)

        self.filename, input_str = OrcaInput.gen_input_orca(self.main_input, self.specific_input, molecule)
        outfile = self.filename + '.out'
        infile = self.filename + '.inp'
        inpath = pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(infile)
        with open(inpath, 'wt') as input_file:
            input_file.write(input_str)
        self.hessian_file = self.filename + '.hess'
        path_hessian = pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(self.hessian_file)
        # path = pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(outfile)
        if not path_hessian.exists():
            self.runner.run(infile, outfile)

        hessian = OrcaOutput.hessian(path_hessian)

        return hessian

    def get_hessian_numerical(self, molecule):
        """Compute analytical hessian with orca."""
        if 'numfreq' not in self.main_input:
            self.main_input.append('numfreq')

        self.filename, input_str = OrcaInput.gen_input_orca(self.main_input, self.specific_input, molecule)
        outfile = self.filename + '.out'
        infile = self.filename + '.inp'
        inpath = pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(infile)
        with open(inpath, 'wt') as input_file:
            input_file.write(input_str)
        self.hessian_file = self.filename + '.hess'
        outpath = pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(outfile)
        if not outpath.exists():
            self.runner.run(infile, outfile)
        path_hessian = pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(self.hessian_file)

        hessian = OrcaOutput.hessian(path_hessian)
        return hessian

    def get_normal_modes(self, molecule):
        """Compute analytical hessian with orca."""
        _ = self.get_hessian(molecule)
        path = pathlib.Path(self.compute_settings.work_dir) / pathlib.Path(self.hessian_file)
        if not path.exists():
            raise Exception(f'could not find hessian outputfile {self.hessian_file}')

        frequencies = OrcaOutput.vibrational_frequencies(path)
        imaginary = []
        for i, freq in enumerate(frequencies):
            if freq < 0:
                imaginary.append(i)
        normal_modes = OrcaOutput.normal_modes(path)

        return {'frequencies': frequencies, 'normal_modes': normal_modes, 'imaginary': imaginary}


if __name__ == '__main__':

    input_str = """!HF
!DEF2-SVP
%SCF
   MAXITER 500
END
* xyz 0 1
O   0.0000   0.0000   0.0626
H  -0.7920   0.0000  -0.4973
H   0.7920   0.0000  -0.4973
*
"""

    # write_file = open('./water.inp', 'w')
    # write_file.write(input_str)
    # write_file.close()

    # molecule = Orca.get_input_molecule('water.inp')
    # method = Orca.get_input_method('water.inp')

    # myorca = Orca(method)
    # input_test = OrcaInput.gen_orca_input_str(method=method, molecule=molecule)
    # inp = myorca.gen_input(method=method, molecule=molecule, filename = 'myfile.inp')

    # test = myorca.run('myfile.inp')
    # en = OrcaOutput.get_energy(test)

    # myorca = Orca({'qcmethod':'uhf', 'properties':'geometry_optimization'})

    # # import hylleraas as hsp
    # mymol = hsp.Molecule('H 0 0 0 \n H 0 0 0.74')
    # mymeth = hsp.Method('qcmethod':'hf', 'basis':'pcseg-1', program = 'orca')
    # print(mymeth)
    # print(test)

    # print('THIS IS THE ORCA METHOD', method)
    # myenv = create_compute_settings(path='/Users/tilmann/Downloads/orca_5_0_3_macosx_intel_openmpi411/')
    # method.update({'compute_settings': myenv})

    # input_str = Orca.generate_input(molecule, method)
    # print(input_str)
    # import hyenv
    # myenv = create_computation_settings(path='/Users/tilmann/Downloads/orca_5_0_3_macosx_intel_openmpi411/')
    # print(method)
    # myorca = Orca(method)
    # output = myorca.run('water.inp')
    # output = myorca.orca.run_async('water.inp')
    # print(output)
    # print(output)
    # testenv = Environment.create_path('/Users/tilmann/Downloads/orca_5_0_3_macosx_intel_openmpi411/')
    # print(testenv, type(testenv))
    # testenv = Environment(bin_dir='/Users/tilmann/Downloads/orca_5_0_3_macosx_intel_openmpi411/')
    # print(testenv)
    # Environment.delete_path('/Users/tilmann/Downloads/orca_5_0_3_macosx_intel_openmpi410/a')
    # Environment.delete_path('/Users/tilmann/Downloads/orca_5_0_3_macosx_intel_openmpi410/')

    # testenv = Environment.path_setter(bin_dir='/Users/tilmann/Downloads/orca_5_0_3_macosx_intel_openmpi411/')
    # print(dir(testenv))
    # runner = Runners.RunnerFileStdout(program='orca', infile='water.inp', env=testenv)
    # test = runner.run()
    # print(test)

    import hylleraas as hsp

    mymol = hsp.Molecule('H 0 0 1 \n H 0 0 -1')
    myenv = hsp.create_compute_settings('local',
                                        path='/Users/tilmann/Downloads/orca_5_0_3_macosx_intel_openmpi411/')
    myorca1 = Orca({'qcmethod': ['uhf', 'D3'], 'properties': 'geometry_optimization', 'compute_settings': myenv})
    test1 = OrcaInput.gen_orca_input_str(myorca1.main_input, myorca1.specific_input, mymol)
    en1 = myorca1.get_energy(mymol)
    print(test1, en1)
    myorca2 = Orca({'main_input': ['HF', 'SVP', 'engrad'], 'specific_input': {'SCF': {'maxiter': 500}}})
    test2 = OrcaInput.gen_orca_input_str(myorca2.main_input, myorca2.specific_input, mymol)
    en2 = myorca2.get_energy(mymol)
    print(test2, en2)
    grad2 = myorca2.get_gradient(mymol)
    print(grad2)
    myorca3 = Orca('water.inp')
    mymol3 = Orca.get_input_molecule('water.inp')
    test3 = OrcaInput.gen_orca_input_str(myorca3.main_input, myorca3.specific_input, mymol3)
    en3 = myorca3.get_energy(mymol3)
    print(test3, en3)
    myorca4 = Orca({'qcmethod': 'uhf', 'properties': 'normal_modes', 'compute_settings': myenv})
    mymol4 = hsp.Molecule({
        'coordinates': [[1.41985512e-14, -1.00353367e-13, -6.05889717e-15],
                        [1.89100353e-11, 1.62032068e-11, -9.24482826e-01],
                        [-1.89239273e-11, -1.61003021e-11, 9.24482826e-01]], 'atoms': ['O', 'H', 'H']
    })
    test4 = myorca4.get_normal_modes(mymol4)
    print(test4)
    myorca5 = Orca({'qcmethod': ['uhf', 'D4'], 'properties': 'geometry_optimization', 'compute_settings': myenv})
    test5 = myorca5.run(mymol4, options={'constrain': ['angle', 1, 0, 2, 106]})
    print(test5)
    myorca1 = Orca({'qcmethod': ['uhf', 'D4'], 'compute_settings': myenv})

    mymol = hsp.Molecule({'atoms': ['H'], 'coordinates': [0, 0, 0], 'properties': {'spin_multiplicity': 2}})

    print(myorca1.get_energy(mymol))
