import pathlib
import re
import warnings
from dataclasses import dataclass
from typing import Any, Dict, List, Tuple, Union

# import basis_set_exchange as bse
import numpy as np
from qcelemental import PhysicalConstantsContext

from ..utils import unique_filename

constants = PhysicalConstantsContext('CODATA2018')


@dataclass
class OrcaMolecule:
    """Interface to hylleraas Molecule class."""

    atoms: List
    coordinates: Union[List, np.array]
    properties: Dict


class OrcaOutput:
    """Orca output class."""

    @classmethod
    def parse(cls, output):
        """Parse orca output."""
        with open(output, 'r') as outfile:
            lines = outfile.readlines()

        result: dict = {}

        result['outfile'] = output
        result['warnings'] = []
        num_atoms = -1
        for i, line in enumerate(lines):
            if 'Number of atoms' in line:
                num_atoms = int(lines[i].split()[4])

            if 'FINAL ENERGY EVALUATION AT THE STATIONARY POINT' in line:
                if 'CARTESIAN COORDINATES (ANGSTROEM)' not in lines[i + 4]:
                    raise Exception('parser error')
                istart = i + 6
                atoms = []
                coordinates = []
                coordinates_a = []
                for j in range(istart, istart + num_atoms):
                    split_line = lines[j].split()
                    atoms.append(split_line[0])
                    xyz_a = [float(split_line[k]) for k in range(1, 4)]
                    xyz = [float(split_line[k]) / constants.bohr2angstroms for k in range(1, 4)]
                    coordinates.append(xyz)
                    coordinates_a.append(xyz_a)

                result['atoms'] = atoms
                result['final_geometry_bohrs'] = coordinates
                result['final_geometry_angstroms'] = coordinates_a

            if 'FINAL SINGLE POINT ENERGY' in line:
                result['energy'] = float(line.split()[4])

            if 'WARNING' in line:
                if 'WARNINGS\n' not in line:
                    result['warnings'].append(line)

        return result

    @classmethod
    def energy(cls, output):
        """Extract energy from output file."""
        with open(output, 'r') as outfile:
            lines = outfile.readlines()

        energies = []
        for i, line in enumerate(lines):
            if 'FINAL SINGLE POINT ENERGY' in line:
                energies.append(float(line.split()[4]))
        if len(energies) > 0:
            energy = energies[-1]
        else:
            warnings.warn(f'could not find energy in file {output}')
            energy = 1e6

        return energy

    @classmethod
    def gradient(cls, output, molecule):
        """Extract gradient from output file."""
        n_atoms = len(molecule.atoms)
        with open(output, 'r') as outfile:
            lines = outfile.readlines()

        gradients = []
        for i, line in enumerate(lines):
            if 'CARTESIAN GRADIENT' in line:
                for j in range(i + 3, i + 3 + n_atoms):
                    gradline = lines[j].split()
                    g = [float(gradline[x]) for x in range(3, 6)]
                    gradients.append(g)

        return np.array(gradients)

    @classmethod
    def hessian(cls, output):
        """Extract hessian from output file."""
        with open(output, 'r') as outfile:
            lines = outfile.readlines()

        for i, line in enumerate(lines):
            if '$hessian' in line:
                dim = int(lines[i + 1])
                hessian = np.zeros((dim, dim))
                for j in range(0, dim//5 + 1):
                    idx_pos = i + 2 + (dim+1) * j
                    idx_str = lines[idx_pos].split()
                    idx = [int(x) for x in idx_str]
                    for k in range(0, dim):
                        curr = lines[idx_pos + 1 + k].split()
                        idy = int(curr[0])
                        vals = [float(curr[ii]) for ii in range(1, len(curr))]
                        for ii, iidx in enumerate(idx):
                            hessian[idy, iidx] = vals[ii]

        return hessian

    @classmethod
    def vibrational_frequencies(cls, output):
        """Extract vibrational_frequencies from output file."""
        with open(output, 'r') as outfile:
            lines = outfile.readlines()

        for i, line in enumerate(lines):
            if '$vibrational_frequencies' in line:
                dim = int(lines[i + 1])
                freq = np.zeros(dim)
                for j in range(dim):
                    curr = lines[i + 2 + j].split()
                    freq[j] = float(curr[1])
        return freq

    @classmethod
    def normal_modes(cls, output):
        """Extract normal_modes from output file."""
        with open(output, 'r') as outfile:
            lines = outfile.readlines()

        for i, line in enumerate(lines):
            if '$normal_modes' in line:
                dim1, dim2 = lines[i + 1].split()
                dim1 = int(dim1)
                dim2 = int(dim2)

                if dim1 != dim2:
                    warnings.warn('dimensions in normal modes do not match')

                modes = np.zeros((dim1, dim2))
                for j in range(0, dim1//5 + 1):
                    idx_pos = i + 2 + (dim1+1) * j
                    idx_str = lines[idx_pos].split()
                    idx = [int(x) for x in idx_str]
                    for k in range(0, dim1):
                        curr = lines[idx_pos + 1 + k].split()
                        idy = int(curr[0])
                        vals = [float(curr[ii]) for ii in range(1, len(curr))]
                        for ii, iidx in enumerate(idx):
                            modes[idy, iidx] = vals[ii]

        return modes.T


class OrcaInput:
    """Orca input class."""

    @classmethod
    def gen_input_orca(cls, main_input, specific_input, molecule):
        """Generate input."""
        input_str = OrcaInput.gen_orca_input_str(main_input, specific_input, molecule)
        filename = unique_filename(input_str.split('\n'))
        # infile = filename + '.inp'

        return filename, input_str

    @classmethod
    def gen_orca_input_str(cls, main_input: list, specific_input: dict, molecule: Any) -> str:
        """Generate orca input string."""
        input_str = ''
        for x in main_input:
            input_str += f'!{x}\n'
        for k, v in specific_input.items():
            input_str += f'%{k.lower()}\n'
            if isinstance(v, dict):
                for k1, v1 in v.items():
                    input_str += f'\t{k1} {v1}\n'
            elif isinstance(v, str):
                input_str += v + '\n'
            elif isinstance(v, list):
                for vv in v:
                    input_str += vv + '\n'
            input_str += 'end\n'

        try:
            if hasattr(molecule, 'atoms'):
                atoms = molecule.atoms
                coords = molecule.coordinates
                if hasattr(molecule, 'properties'):
                    prop = molecule.properties
                else:
                    prop = {}
            elif hasattr(molecule, '_atoms'):
                atoms = molecule._atoms
                coords = molecule._coordiantes
                if hasattr(molecule, '_properties'):
                    prop = molecule._properties
                else:
                    prop = {}
        except Exception:
            raise Exception(f'could not read molecule {molecule}')

        charge = 0
        spin = 1

        # charge = prop.setdefault('charge', 0)
        # spin = prop.setdefault('spin_multiplicity',1)

        if 'charge' in prop:
            charge = prop['charge']
        if 'spin_multiplicity' in prop:
            spin = prop['spin_multiplicity']

        input_str += f'* xyz {str(charge)} {str(spin)}\n'

        scale = 1.0
        if len(atoms) == 1 and len(coords) == 3:
            coords = [coords]

        for i, atom in enumerate(atoms):
            input_str += (2) * ' ' + '{:>2}{:20.14f}{:20.14f}{:20.14f}\n'.format(
                atoms[i],
                # replace bny numpy arrays
                coords[i][0] * scale,
                coords[i][1] * scale,
                coords[i][2] * scale,
            )
        input_str += '*\n'

        return input_str

    @classmethod
    def read_molecule_from_file(cls, filename: str) -> dict:
        """Read mol from file."""
        try:
            path = pathlib.Path(filename)
        except Exception:
            raise Exception('orca in put must be either dict or filename')
        else:
            if not path.exists():
                raise Exception(f'could not find inputfile {filename}')

        with open(f'{path}', 'r') as in_file:
            lines = in_file.readlines()

        stack: list = []
        atoms: list = []
        coordinates: list = []
        for line in lines:
            line_split = line.split()
            if line_split[0] == '*':
                stack.append('*')
                if len(stack) == 2:
                    break
                charge = line_split[2]
                multiplicity = line_split[3]
                continue
            if len(stack) == 1:
                atoms.append(line_split[0])
                coordinates.append(line_split[1:4])

            # add unit check here

        coordinates = np.array(coordinates).astype(np.float)
        coordinates *= 1.0 / constants.bohr2angstroms

        mol_dict: dict = {
            'atoms': atoms, 'coordinates': coordinates,
            'properties': {'charge': charge, 'spin_multiplicity': multiplicity}
        }
        return mol_dict

    @classmethod
    def read_method_from_file(cls, filename: str) -> Tuple[List, Dict]:
        """Read method from file."""
        # input_dict: dict = {}
        main_input: list = []
        specific_input: dict = {}

        try:
            path = pathlib.Path(filename)
        except Exception:
            raise Exception('orca in put must be either dict or filename')
        else:
            if not path.exists():
                raise Exception(f'could not find inputfile {filename}')

        with open(f'{path}', 'r') as in_file:
            lines = in_file.readlines()
        # # input_dict['filename'] = unique_filename(lines)
        # input_dict['filename'] = filename

        stack = []
        for line in lines:
            line_split = line.split()
            line_dsplit = re.split('(?<=[!%])', line_split[0])
            if line_dsplit[0] == '!':
                # main input
                main_input.append(line_dsplit[1])  # what about more keywords? is that possible in orca?
            elif line_dsplit[0] == '%':
                # special input
                stack.append(line_dsplit[1])
                mydict: dict = {}
                continue

            if line_split[0].lower() == 'end':
                key = stack.pop()
                specific_input[key] = mydict

            if len(stack) > 0:
                mydict[line_split[0]] = line_split[1]  # what about more keywords? is that possible in orca?

        # for kw in main_input:
        #     if kw.lower() in COMP_METHODS:
        #         input_dict['qcmethod'] = kw.lower()
        #     elif kw.lower() in RUNTYPES:
        #         input_dict['properties'] = kw.lower()
        #     else:
        #         try:
        #             bse.get_basis(name=kw)
        #         except Exception:
        #             continue
        #         else:
        #             input_dict['basis'] = kw

        # input_dict['specific_input'] = specific_input
        # input_dict['main_input'] = main_input
        # return input_dict

        return main_input, specific_input
