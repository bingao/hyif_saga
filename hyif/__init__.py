from .cp2k import CP2K
from .dalton import Dalton
from .dummy import Dummy, Dummy2
from .london import London
from .lsdalton import LSDalton
from .mrchem import MRChem
from .orca import Orca
from .xtb import Xtb

PROGRAMS: dict = {
    'dummy': 'Dummy', 'dummy2': 'Dummy2', 'dalton': 'Dalton', 'london': 'London', 'lsdalton': 'LSDalton',
    'mrchem': 'MRChem', 'orca': 'Orca', 'xtb': 'Xtb', 'cp2k': 'CP2K'
}
# EXTENSIONS: dict = {'dummy': 'Dummy', 'dummy2': 'Dummy2', 'dal': 'Dalton'}
QC_PROGS: list = ['dummy', 'dalton', 'lsdalton', 'london', 'orca', 'xtb', 'cp2k']
